"""test_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.http import HttpResponseForbidden, HttpResponse
from django.urls import path

from throttler.logic import throttle_response
from throttler.utils.decorators import throttle_exempt


def test_403(request):
    return HttpResponseForbidden()


def test_marked(request):
    return throttle_response(HttpResponse())


@throttle_exempt
def test_exempt(request):
    return test_403(request)


urlpatterns = [
    path('403/', test_403),
    path('marked/', test_marked),
    path('exempt/', test_exempt),
    path('admin/', admin.site.urls),
]
