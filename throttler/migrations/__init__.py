from django.db.migrations import Migration

from throttler.settings import is_using_orm


class ConditionalMigration(Migration):
    """
    A migration which is only applied when the ModelDataInterface handler is
    set. Otherwise, this migration will be applied by printing to the console
    that this migration is "applied"
    """

    def apply(self, project_state, *args, **kwargs):
        if is_using_orm():
            return super().apply(project_state, *args, **kwargs)
        else:
            print(' Migration in reality not actually applied as a '
                  'non-default handler is used', end='')
            return project_state
