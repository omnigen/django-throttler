import inspect
from typing import List

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.module_loading import import_string

from throttler.handler.data_interface import DataInterface
from throttler.utils.decorators import cache_result


def validate():
    """
    Retrieves all settings. By retrieving all settings these are also
    validated, and since these settings are being cached this doesn't cause
    double validation.

    :raises ImproperlyConfigured: When a setting failed to validate
    """
    get_invalid_status_codes()
    get_throttle_cooldown()
    get_throttle_max_sleep()
    failed_minimum = get_throttle_failed_minimum()
    ban_threshold = get_ban_threshold()
    get_ban_time()
    get_permanent_ban_threshold()
    get_data_handler()
    if failed_minimum >= ban_threshold:
        raise ImproperlyConfigured('THROTTLE_FAILED_MINIMUM should be less '
                                   'than THROTTLE_BAN_THRESHOLD')


@cache_result
def get_invalid_status_codes() -> List[int]:
    """
    Retrieves the status codes on which the throttler will automatically
    activate if necessary. The setting THROTTLE_STATUS_CODES should be a
    list of ints, representing the status codes. THROTTLE_STATUS_CODES can
    also be an int, representing a single status code. This setting defaults
    to the status codes 401 and 403.

    :return: The status codes to automatically trigger the throttler on
    :rtype: List[int]
    :raises ImproperlyConfigured: When the setting is invalid
    """
    invalid_status_codes = getattr(
        settings, 'THROTTLE_STATUS_CODES', [401, 403])
    if isinstance(invalid_status_codes, int):
        invalid_status_codes = [invalid_status_codes]
    elif not isinstance(invalid_status_codes, list):
        raise ImproperlyConfigured(
            'THROTTLE_STATUS_CODES should be a list or int')
    return invalid_status_codes


def _get_int_setting(name: str, default: int) -> int:
    """
    Retrieves a setting from the django conf, sets the default and validates
    whether the value is actually an int and > 0.

    :param name: The name of the setting to retrieve
    :type name: str
    :param default: The default value of the setting
    :type default: int

    :return: The configured value
    :rtype: int
    :raises ImproperlyConfigured: When the setting is invalid (not an int or \
        <= 0)
    """
    setting_value = getattr(settings, name, default)
    try:
        setting_value = int(setting_value)
    except ValueError:
        raise ImproperlyConfigured(f'{name} is not an int')
    if setting_value <= 0:
        raise ImproperlyConfigured(f'{name} should be > 0')
    return setting_value


@cache_result
def get_throttle_cooldown() -> int:
    """
    Retrieves the cooldown period which is used to determine whether a brute
    force is applied or not. This THROTTLE_COOLDOWN setting is expressed in
    seconds and defaults to a minute.

    :return: The cooldown period in seconds
    :rtype: int
    :raises ImproperlyConfigured: When the setting is invalid (not an int or \
        <= 0)
    """
    return _get_int_setting('THROTTLE_COOLDOWN', 60)


@cache_result
def get_throttle_max_sleep() -> int:
    """
    Retrieves the maximum sleep time in seconds when throttling.
    THROTTLE_MAX_SLEEP defaults to 30 seconds.

    :return: The amount of seconds the throttler can sleep at a maximum
    :rtype: int
    :raises ImproperlyConfigured: When the setting is invalid (not an int or \
        <= 0)
    """
    return _get_int_setting('THROTTLE_MAX_SLEEP', 30)


@cache_result
def get_throttle_failed_minimum() -> int:
    """
    Retrieves the amount of minimum brute-force detected requests before
    actually applying a throttle. THROTTLE_FAILED_MINIMUM should not be set
    too low, as it can occur that users get forbidden responses time to time.
    This setting defaults to a failed minimum of 5 requests.

    :return: The amount of minimum brute-force detected requests before \
        throttling
    :rtype: int
    :raises ImproperlyConfigured: When the setting is invalid (not an int or \
        <= 0)
    """
    return _get_int_setting('THROTTLE_FAILED_MINIMUM', 5)


@cache_result
def get_ban_threshold() -> int:
    """
    Retrieves the amount of minimum brute-force detected requests before
    temporarily banning the IP address. THROTTLE_BAN_THRESHOLD defaults to
    50 requests.

    :return: The threshold of brute-force attempts to ban an IP address
    :rtype: int
    :raises ImproperlyConfigured: When the setting is invalid (not an int or \
        <= 0)
    """
    return _get_int_setting('THROTTLE_BAN_THRESHOLD', 50)


@cache_result
def get_ban_time() -> int:
    """
    Retrieves the amount of seconds a ban should be applied when an IP reached
    the ban threshold. THROTTLE_BAN_TIME defaults to day (86400 seconds).

    :return: The amount of seconds to ban an IP for
    :rtype: int
    :raises ImproperlyConfigured: When the setting is invalid (not an int or \
        <= 0)
    """
    return _get_int_setting('THROTTLE_BAN_TIME', 86400)


@cache_result
def get_permanent_ban_threshold() -> int:
    """
    Retrieves the amount of times a temporary ban can be applied, before making
    it permanent. THROTTLE_PERMANENT_BAN defaults to 3 times.

    :return: The amount of times a temporary ban may be applied, before \
        making it permanent.
    :rtype: int
    :raises ImproperlyConfigured: When the setting is invalid (not an int or \
        <= 0)
    """
    return _get_int_setting('THROTTLE_PERMANENT_BAN', 3)


@cache_result
def get_data_handler() -> DataInterface:
    """
    Retrieves the configured data interface which handles the storage of
    data (THROTTLE_DATA_HANDLER). This defaults to
    throttler.handler.model_data_interface.ModelDataInterface, which means
    that migrations have to be applied.

    :return: The handler to interface with the saved data
    :rtype: DataInterface
    :raises ImportError: When the configured option could not be imported
    :raises ImproperlyConfigured: When the import is not a class or does not \
        inherit from DataInterface
    """
    handler = getattr(
        settings, 'THROTTLE_DATA_HANDLER',
        'throttler.handler.model_data_interface.ModelDataInterface')
    class_handler = import_string(handler)
    if not inspect.isclass(class_handler):
        raise ImproperlyConfigured(f'{handler} is not a class!')
    if not issubclass(class_handler, DataInterface):
        raise ImproperlyConfigured(
            f'{handler} does not inherit from DataInterface!')
    return class_handler()


def is_using_orm() -> bool:
    """
    Determines whether the data handler uses the Django's ORM manager

    :return: Whether the ORM data handler is user or not
    :rtype: bool
    """
    from throttler.handler.model_data_interface import ModelDataInterface

    return get_data_handler().__class__ == ModelDataInterface
