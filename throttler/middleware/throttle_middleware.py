from typing import Callable, Dict, List

import time
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponse

from throttler import logic, settings
from throttler.utils import get_ip
from throttler.utils.responses import HttpResponseTooManyRequests

settings.validate()


class ThrottleMiddleware(object):
    """
    Middleware which throttles requests when a lot of invalid requests are
    detected from a single IP and eventually temporarily bans this.
    """

    def __init__(self, get_response: Callable):
        self._get_response = get_response
        self._invalid_status_codes = settings.get_invalid_status_codes()

    def _process_request(self, request: WSGIRequest,
                         ip_address: str) -> HttpResponse:
        """
        Processes a request

        :param request: The request to check
        :type request: WSGIRequest
        :param ip_address: The ip address of the request
        :type ip_address: str

        :return: The response which is either custom or returned by the view
        :rtype: HttpResponse
        """
        request.throttle_processing = True
        if logic.is_ip_banned(ip_address):
            del settings.get_data_handler()[ip_address]
            return HttpResponseTooManyRequests()
        return self._get_response(request)

    def process_view(self, request: WSGIRequest, callback: Callable,
                     callback_args: List, callback_kwargs: Dict) -> None:
        if not getattr(request, 'throttle_processing', False):
            return
        if getattr(callback, logic.THROTTLE_EXEMPT_FUNC_ATTR, False):
            request.throttle_processing = False

    def _process_response(self, response: HttpResponse,
                          ip_address: str) -> HttpResponse:
        """
        Processes the response. When either a 401 or 403 is returned on the
        http response, add it to the counter and if necessary even sleep a
        bit.

        :param response: The response to process
        :type response: HttpResponse
        :param ip_address: The ip address of the request
        :type ip_address: str

        :return: The processed or original response
        :rtype: HttpResponse
        """
        force_logic = getattr(response, logic.THROTTLE_FORCE_MARKER, False)
        if force_logic or response.status_code in self._invalid_status_codes:
            sleeping_time = logic.get_sleeping_period(ip_address)
            if sleeping_time != 0:
                time.sleep(sleeping_time)

        return response

    def __call__(self, request: WSGIRequest) -> HttpResponse:
        ip_address = get_ip(request)
        response = self._process_request(request, ip_address)
        if request.throttle_processing:
            return self._process_response(response, ip_address)
        return response
