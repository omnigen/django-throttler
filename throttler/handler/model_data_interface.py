from datetime import datetime
from typing import Optional

from pytz import UTC

from throttler.handler.data_interface import DataInterface, DataEntity
from throttler.models import InvalidAccess


class ModelDataInterface(DataInterface):
    """
    A DataInterface based on the default entity defined in this app
    """

    def _get_entity(self, ip_address: str) -> Optional[InvalidAccess]:
        try:
            return InvalidAccess.objects.get(ip_address=ip_address)
        except InvalidAccess.DoesNotExist:
            pass

    def retrieve_data(self, ip_address: str) -> Optional[object]:
        invalid_access = self._get_entity(ip_address)
        if invalid_access:
            return DataEntity(
                invalid_access.count, invalid_access.update_time.timestamp(),
                invalid_access.temp_bans)

    def set_data(self, ip: str, count: int, timestamp: int,
                 temp_bans: int) -> None:
        invalid_access = (
                self._get_entity(ip) or InvalidAccess(ip_address=ip))
        invalid_access.count = count
        invalid_access.update_time = datetime.fromtimestamp(timestamp, UTC)
        invalid_access.temp_bans = temp_bans
        invalid_access.save()
