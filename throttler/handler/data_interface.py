from collections import namedtuple
from typing import Optional

from django.conf import settings


class DataInterface(object):
    """
    The interface to retrieve and set data which is necessary for throttling
    requests.
    """

    def __init__(self):
        self._cache = dict()

    def _cached_retrieve_data(self, ip_address: str) -> Optional[object]:
        """
        Retrieves the data for an IP address similar to retrieve data. First
        attempts to consult the cache before invoking original function.
        """
        if ip_address in self._cache:
            return self._cache[ip_address]
        self._cache[ip_address] = self.retrieve_data__bypass(ip_address)
        return self._cache[ip_address]

    def _cached_set_data(self, ip_address: str, *args) -> None:
        """
        Removes IP address from the cache if it was in there
        """
        del self[ip_address]
        return self.set_data__bypass(ip_address, *args)

    def retrieve_data(self, ip_address: str) -> Optional[object]:
        """
        Retrieves the data for an IP address. If no data exists for this
        ip address, simply return None. When data exists, an object of any
        type should be returned with at least three attributes:
        - count -> int
        - timestamp -> int
        - temp_bans -> int
        The throttler.handler.data_interface.DataEntity can be used to
        transform any type of data to the desired object. This is not forced
        though.

        :param ip_address: The IP address to retrieve the data for
        :type ip_address: str

        :return: An object with at least the attributes count and timestamp, \
            or None when the data is not available.
        :rtype: object
        """
        raise NotImplementedError

    def set_data(self, ip_address: str, count: int, timestamp: int,
                 temp_bans: int) -> None:
        """
        Saves the count and timestamp associated with a timestamp. This either
        should create a new entity or update an existing entity.

        :param ip_address: The IP address to set the data for
        :type ip_address: str
        :param count: The count to set
        :type count: int
        :param timestamp: The timestamp to set
        :type timestamp: int
        :param temp_bans: The amount of times this IP has already been \
            temporarily banned
        :type temp_bans: int
        """
        raise NotImplementedError

    def __delitem__(self, key: str):
        if key in self._cache:
            del self._cache[key]

    def __getattribute__(self, item: str):
        if settings.DEBUG:
            # Don't use a cache when debugging
            return super().__getattribute__(item)

        if item.endswith('__bypass'):
            return super().__getattribute__(item[:-8])
        try:
            return super().__getattribute__('_cached_' + item)
        except AttributeError:
            return super().__getattribute__(item)


DataEntity = namedtuple('Entity', ['count', 'timestamp', 'temp_bans'])
