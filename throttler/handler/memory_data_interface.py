from typing import Optional

from throttler.handler.data_interface import DataInterface, DataEntity


class MemoryDataInterface(DataInterface):
    """
    A DataInterface which saves the data in-memory, meaning that this is not
    a persistent form of the date interface.
    """

    def __init__(self):
        super().__init__()
        self._data = dict()

    def retrieve_data(self, ip_address: str) -> Optional[object]:
        if ip_address in self._data:
            data = self._data[ip_address]
            return DataEntity(
                data['count'], data['timestamp'], data['temp_bans'])

    def set_data(self, ip_address: str, count: int, timestamp: int,
                 temp_bans: int) -> None:
        if ip_address not in self._data:
            self._data[ip_address] = dict()
        data = self._data[ip_address]
        data['count'] = count
        data['timestamp'] = timestamp
        data['temp_bans'] = temp_bans
