from django.apps import AppConfig


class ThrottlerConfig(AppConfig):
    name = 'throttler'
