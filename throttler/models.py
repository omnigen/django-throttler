from django.db import models
from django.utils.translation import gettext_lazy as _


class InvalidAccess(models.Model):
    ip_address = models.GenericIPAddressField(_('IP address'), db_index=True)

    count = models.PositiveIntegerField(_('Invalid access count'), default=0)
    update_time = models.DateTimeField(_('Update time'))
    temp_bans = models.PositiveIntegerField(_('Temporary bans'), default=0)

    class Meta:
        app_label = 'throttler'
        ordering = ['-update_time']
