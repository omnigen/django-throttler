from django.contrib import admin

from throttler.models import InvalidAccess
from throttler.settings import is_using_orm

if is_using_orm():
    @admin.register(InvalidAccess)
    class InvalidAccessAdmin(admin.ModelAdmin):
        readonly_fields = [
            'ip_address'
        ]
