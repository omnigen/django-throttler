from django.http import HttpResponse
from math import sqrt

from throttler import settings
from throttler.utils import get_current_timestamp

THROTTLE_EXEMPT_FUNC_ATTR = 'throttle_exempt'
THROTTLE_EXEMPT_MARKER = '_throttle_exempt'
THROTTLE_FORCE_MARKER = '_throttle_force'


def throttle_response(response: HttpResponse) -> HttpResponse:
    """
    Marks the response as it should be throttled (unless the view which
    returns this response is exempt for throttling) without setting an
    invalid status code. This modification is applied in place, the returned
    response object is the same object with an additional marker.

    :param response: The response which should be throttled
    :type response: HttpResponse

    :return: The same response marker to process by the throttler
    :rtype: HttpResponse
    """
    setattr(response, THROTTLE_FORCE_MARKER, True)
    return response


def is_ip_banned(ip_address: str) -> bool:
    """
    Checks whether the IP address is banned or not

    :param ip_address: The IP address to check
    :type ip_address: str

    :return: Whether the IP address is banned or not
    :rtype: bool
    """
    ip_address_data = settings.get_data_handler().retrieve_data(ip_address)
    if ip_address_data:
        if ip_address_data.temp_bans >= settings.get_permanent_ban_threshold():
            return True

        current_timestamp = get_current_timestamp()
        ban_time = settings.get_ban_time()
        if current_timestamp - ip_address_data.timestamp <= ban_time:
            return ip_address_data.count >= settings.get_ban_threshold()
    return False


def get_sleeping_period(ip_address: str) -> float:
    """
    Determines whether the current request has to be throttled for the given
    IP address, and calculates the associated sleeping time.

    :param ip_address: The IP address to check
    :type ip_address: str

    :return: The amount of seconds the ip address should be throttled for
    :rtype: float
    """
    count = 1
    temp_bans = 0
    current_timestamp = get_current_timestamp()

    data_handler = settings.get_data_handler()
    ip_address_data = data_handler.retrieve_data(ip_address)
    if ip_address_data:
        throttle_cooldown = settings.get_throttle_cooldown()
        if current_timestamp - ip_address_data.timestamp <= throttle_cooldown:
            count = ip_address_data.count
            if count >= settings.get_ban_threshold():
                count = 1
            else:
                count += 1
                if count >= settings.get_ban_threshold():
                    temp_bans = ip_address_data.temp_bans + 1

    data_handler.set_data(ip_address, count, current_timestamp, temp_bans)
    return calculate_sleeping_period(count)


def calculate_sleeping_period(count: int) -> float:
    """
    Calculates the amount of seconds the request should sleep for before
    returning. The amount of seconds are float, because time.sleep is able to
    handle floats properly. This calculation is based on a quadratic function,
    so that brute-forces has to incrementally wait longer for a request to
    resolve.

    First a check is applied whether the minimum amount of requests are
    detected before sleeping. When this minimum is no hit, this will simply
    return 0. Otherwise, the following calculation will be applied:

    Solve x for the maximum amount a sleep can take, which is used as maximum
    x value. Then based on the current request count and ban threshold, a
    percentage is calculated which is then multiplied with the maximum x to get
    a relative x value. This value is then put back into the original quadratic
    function, x^2, to calculate the amount of seconds the request should be
    slept for.

    :param count: The amount of brute-force based requests a single IP made
    :type count: int

    :return: The amount of seconds the request should sleep for
    :rtype: float
    """
    if count < settings.get_throttle_failed_minimum():
        return 0

    max_x = sqrt(settings.get_throttle_max_sleep())
    percentage = count / settings.get_ban_threshold()
    relative_x = percentage * max_x
    return relative_x ** 2
