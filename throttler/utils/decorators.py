from functools import wraps
from typing import Callable, Optional


def cache_result(func: Callable) -> Callable:
    """
    Caches results of simple functions. Simple functions are functions which
    don't have any arguments (and therefore the result is constant).
    """

    @wraps(func)
    def wrapper():
        cache = getattr(func, 'cache', None)
        if cache is None:
            func.cache = func()
        return getattr(func, 'cache')

    return wrapper


def throttle_exempt(original_func: Optional[Callable] = None,
                    csrf_exempt: bool = False,
                    xframe_options_exempt: bool = False) -> Callable:
    """
    A decorator which makes the request exempt to the throttling mechanism

    :param original_func: The function this decorator decorates. Is not \
        always available, depending on how this decorator is used.
    :type func: callable
    :param csrf_exempt: Whether this endpoint should also be csrf exempt
    :type csrf_exempt: bool
    :param xframe_options_exempt: Whether this endpoint should also be xframe \
        options exempt
    :type xframe_options_exempt: bool
    """
    from throttler.logic import THROTTLE_EXEMPT_FUNC_ATTR

    def decorator(func: Callable) -> Callable:
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        wrapper.csrf_exempt = csrf_exempt
        wrapper.xframe_options_exempt = xframe_options_exempt
        setattr(wrapper, THROTTLE_EXEMPT_FUNC_ATTR, True)
        return wraps(func)(wrapper)

    if original_func:
        return decorator(original_func)
    else:
        return decorator
