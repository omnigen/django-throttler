from datetime import datetime

from django.core.handlers.wsgi import WSGIRequest


def get_ip(request: WSGIRequest) -> str:
    """
    Retrieves the IP address of the request

    :param request: The request to retrieve the IP from
    :type request: WSGIRequest

    :return: The IP address
    :rtype: str
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0].strip()
    else:
        ip = request.META.get('REMOTE_ADDR', '')
    return ip


def get_current_timestamp() -> int:
    """
    Retrieves the current timestamp

    :return: The current timestamp
    :rtype: int
    """
    return int(datetime.utcnow().timestamp())
