from setuptools import setup, find_packages

packages = list(filter(
    lambda package: not package.startswith('test_project'),
    find_packages()))


setup(name='django-throttler',
      version='0.1',
      description='A middleware which allows requests to be throttled when a '
                  'lot of invalid requests are made to the server and eventua'
                  'lly temporarily banned',
      url='https://bitbucket.org/omnigen/django-throttler',
      author='Wesley Ameling',
      author_email='w.ameling@omnigen.nl',
      license='MIT',
      packages=packages,
      install_requires=[
          'django>=2.0'
      ],
      classifiers=[
          'Environment :: Web Environment',
          'Framework :: Django',
          'Framework :: Django :: 2.1',
          'Framework :: Django :: 2.2',
          'Programming Language :: Python',
          'Programming Language :: Python :: 3.7',
          'Topic :: Security',
      ],
      zip_safe=False)
