# django-throttler

Django-throttler throttles requests when they return an invalid response. By default, invalid responses are defined as response which have a status code of 401 or 403. Responses can also be marked as invalid manually, by calling `throttler.logic.throttle_response(response)`.

The persistent data is stored by default in the configured database, but can be overridden (refer to the Configuration section). The data handler has been abstracted, and currently two implementations are available:

1. `ModelDataInterface`: Makes use of entities in Django's ORM
1. `MemoryDataInterface`: Makes us of an in-memory storage, which is not persistent and therefore probably only useful for testing.

Both these objects implement `DataInterface`, which only contains two methods:

1. `#retrieve_data(ip_address)`: Return an object with count (int), timestamp (int) and temp_bans (int) attributes if found. Otherwise return None.
1. `#set_data(ip_address, count, timestamp, temp_bans)`: Save, ideally persist, the data for the interface

## Installation

Installation is very easy:
```bash
pip install .
```

If you want to make use of the default data interface (which is based on Django's ORM), you have to add the app your `INSTALLED_APPS` setting:
```python
INSTALLED_APPS = [
    ...
    'throttler'
]
```

It is required to add the middleware to the `MIDDLEWARE` settings, otherwise this library is not able to throttle requests:
```python
MIDDLEWARE = [
    ...
    'throttler.middleware.ThrottleMiddleware'
]
```

It is recommended to apply this middleware as the last on in the list, so that other middleware's can reject the response for any reason.

## Configuration

The following settings for this app are available (but all have sensible defaults):

* `THROTTLE_DATA_HANDLER`: The import string which is used as data handler. Defaults to `throttler.handler.model_data_interface.ModelDataInterface`.
* `THROTTLE_STATUS_CODES`: A list of status codes which are considered invalid. Defaults to `[401, 403]`.
* `THROTTLE_COOLDOWN`: Cooldown period in seconds which used whether a request can be considered as brute force. Defaults to 60.
* `THROTTLE_MAX_SLEEP`: The maximum period in seconds the throttler may sleep. The sleeping period is calculated based on quadratic function considering `THROTTLE_FAILED_MINIMUM` and `THROTTLE_BAN_THRESHOLD`. Defaults to 30.
* `THROTTLE_FAILED_MINIMUM`: The minimum amount of requests, which are considered brute-force, to start throttling after. Defaults to 5.
* `THROTTLE_BAN_THRESHOLD`: The amount of brute-force requests until an IP address is temporarily banned. Defaults to 50.
* `THROTTLE_BAN_TIME`: The amount of seconds for which a temporary ban lasts.
* `THROTTLE_PERMANENT_BAN`: The amount of times a temporary ban can be applied, before making it permanent. Defaults to 3.

# Development

This project comes with a test project, to test the middleware. Run the following commands to get the development server running:

```bash
python test_project/manage.py migrate
python test_project/manage.py runserver
```

Do note that it is recommended to have the throttler application installed through the following command:
```bash
pip install -e .
```

This way of installing a package ensures that changes in the application will directly be reflected in the current installation, without reinstalling.

This test server uses a SQLite database, meaning that there is no real dependency on MySql. Linux usually comes with an SQLite installation by default, not sure about MacOS and Windows. [SQLitebrowser](https://sqlitebrowser.org/) can be used to review changes in the database for validation purposes.

Finally, 3 URL's are available on the test server:

1. `/403`: Returns a HttpForbiddenResponse
1. `/marked`: Returns a Response which has been marker as invalid
1. `/exempt`: Returns a HttpForbiddenResponse but is exempt from throttling

In the future, this project should contain the tests which should eventually be available.
